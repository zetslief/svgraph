use std::collections::HashMap;
use std::fmt::Display;

pub type Id = usize;

pub struct Node<T> {
    pub id: Id,
    pub data: T,
}

pub struct Edge<T> {
    pub head: Id,
    pub tail: Id,
    pub data: T,
}

pub struct Graph<N, E> {
    pub nodes: Vec<Node<N>>,
    pub edges: Vec<Edge<E>>,
}

impl<T> Display for Node<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{} | {}]", self.id, self.data)
    }
}

impl<T> Display for Edge<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{} -> {} | {}]", self.head, self.tail, self.data)
    }
}

impl<N, E> Display for Graph<N, E> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Graph(N: {}, E: {})", self.nodes.len(), self.edges.len())
    }
}

pub fn find_directional_neighbours<E>(
    source: Id,
    edges: &[Edge<E>],
) -> impl Iterator<Item = (Id, &Edge<E>)> + '_ {
    edges
        .iter()
        .filter(move |edge| edge.head == source)
        .map(|edge| (edge.tail, edge))
}

pub fn find_bidirectional_neighbours<E>(
    source: Id,
    edges: &[Edge<E>],
) -> impl Iterator<Item = (Id, &Edge<E>)> + '_ {
    edges.iter().filter_map(move |edge| {
        if edge.head == source {
            Some((edge.tail, edge))
        } else if edge.tail == source {
            Some((edge.head, edge))
        } else {
            None
        }
    })
}

pub fn breadth_first_search<'e, E, I, F>(
    source: Id,
    edges: &'e [Edge<E>],
    find_neighbours: F,
) -> HashMap<Id, Id>
where
    I: Iterator<Item = (Id, &'e Edge<E>)>,
    F: Fn(Id, &'e [Edge<E>]) -> I,
{
    use std::collections::{HashSet, VecDeque};
    let mut queue = VecDeque::new();
    let mut result = HashMap::new();
    let mut visited = HashSet::new();
    queue.push_back((source, source));
    while queue.len() > 0 {
        let (parent, current) = queue.pop_front().unwrap();
        visited.insert(current);
        find_neighbours(current, edges).for_each(|(neighbour, _e)| {
            if !visited.contains(&neighbour) {
                visited.insert(neighbour);
                queue.push_back((current, neighbour))
            }
        });
        result.insert(current, parent);
    }
    result
}

pub fn branching_factor<'a, N, E, F, I>(
    graph: &'a Graph<N, E>,
    searcher: F
) -> usize 
where 
    I: Iterator<Item=(Id, &'a Edge<E>)>,
    F: Fn(Id, &'a [Edge<E>]) -> I
{
    let mut acc = 0;
    for node in &graph.nodes {
        acc += searcher(node.id, &graph.edges).count();
    }
    acc / graph.nodes.len()
}

pub fn build_shortest_path(source: Id, destination: Id, bfs: HashMap<Id, Id>) -> Option<Vec<Id>> {
    let mut result = Vec::new();
    let mut next = destination;
    loop {
        result.push(next);
        next = *bfs.get(&next)?;
        if next == source {
            result.push(next);
            break;
        }
    }
    result.reverse();
    Some(result)
}

pub fn build_all_pathes<E>(source: Id, destination: Id, edges: &[Edge<E>], mut bfs: HashMap<Id, Id>) -> Vec<Vec<Id>> {
    let mut result = Vec::new();
    let mut neighbours = find_bidirectional_neighbours(source, edges).collect::<Vec<_>>();
    let mut current = source;
    while neighbours.len() > 0 {
        let Some(path) = build_shortest_path(current, destination, bfs.clone()) else {
            bfs.remove(&current);
            println!("Failed find path for {current}...");
            current = neighbours.pop().unwrap().0;
            continue;
        };
        bfs.remove(&current);
        neighbours = neighbours.into_iter().filter(|e| path.iter().find(|r| **r == e.0).is_none()).collect();
        println!("{:?}", neighbours.iter().map(|e| e.0).collect::<Vec<_>>());
        if neighbours.len() == 0 {
            break;
        }
        current = neighbours.pop().expect("Vector is not empty").0;
        result.push(path);
    };
    result
}
