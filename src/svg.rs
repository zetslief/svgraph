use std::io;
use std::io::Write;

pub struct SvgBuilder<W: Write> {
    writer: W,
}

impl<W: Write> SvgBuilder<W> {
    pub fn new(mut writer: W, width: usize, height: usize) -> Result<Self, io::Error> {
        write!(
            &mut writer,
            "<svg width=\"{width}\" height=\"{height}\" xmlns=\"http://www.w3.org/2000/svg\">\n"
        )?;
        Ok( Self { writer })
    }

    pub fn circle(
        &mut self,
        cx: usize,
        cy: usize,
        radius: usize,
        stroke: &'static str,
        fill: &'static str,
    ) -> Result<&mut Self, io::Error> {
        write!(
            &mut self.writer,
           "<circle cx=\"{cx}\" cy=\"{cy}\" r=\"{radius}\" fill=\"{fill}\" stroke=\"{stroke}\" />\n"
        )?;
        Ok(self)
    }

    pub fn line(
        &mut self,
        from_x: usize,
        from_y: usize,
        to_x: usize,
        to_y: usize,
        width: usize,
        stroke: &'static str,
    ) -> Result<&mut Self, io::Error> {
        write!(
            &mut self.writer,
            "<line stroke=\"{stroke}\" x1=\"{from_x}\" y1=\"{from_y}\" x2=\"{to_x}\" y2=\"{to_y}\" stroke-width=\"{width}\" />\n"
        )?;
        Ok(self)
    }

    pub fn build(mut self) -> Result<W, io::Error> {
        write!(&mut self.writer, "</svg>")?;
        Ok(self.writer)
    }
}
