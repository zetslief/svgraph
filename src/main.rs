#![feature(str_split_as_str)]

mod graph;
mod svg;

use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::path::Path;
use std::time;

use graph::{
    breadth_first_search, build_shortest_path, find_bidirectional_neighbours,
    find_directional_neighbours, Edge, Graph, Id, Node,
};

struct Image {
    data: Vec<u8>,
    width: usize,
    height: usize,
}

struct PixelData {
    distance: f32,
    height: i16,
}

fn read_image(image: &Path) -> Result<Image, png::DecodingError> {
    let image = png::Decoder::new(File::open(image)?);
    let mut reader = image.read_info().unwrap();
    let mut image_buffer = vec![0; reader.output_buffer_size()];
    reader
        .next_frame(&mut image_buffer)
        .map(|output_info| Image {
            data: image_buffer,
            width: output_info.width as usize,
            height: output_info.height as usize,
        })
}

fn write_image<E>(
    path: &Path,
    width: usize,
    search_index: usize,
    nodes: &[Node<u8>],
    edges: &[Edge<E>],
    pixels: Vec<u8>,
    bfs_path: HashMap<Id, Id>,
    shortest_path: Option<Vec<usize>>,
) -> Result<(), io::Error> {
    use std::io::Write;
    let file = File::create(path)?;
    let rows = pixels.len() / width;
    let columns = width;
    let node_radius = 10;
    let offset = 1;
    let scale = 30;
    let total_width = (columns + 1 * offset) * scale;
    let total_height = (rows + 1 * offset) * scale;
    let mut builder = svg::SvgBuilder::<_>::new(file, total_width, total_height)?;
    for (current_node, parent_node) in bfs_path.iter() {
        let (mut current_cx, mut current_cy) = from_flat_index(*current_node, width);
        let (mut parent_cx, mut parent_cy) = from_flat_index(*parent_node, width);
        current_cx += offset;
        current_cy += offset;
        parent_cx += offset;
        parent_cy += offset;
        current_cx *= scale;
        current_cy *= scale;
        parent_cx *= scale;
        parent_cy *= scale;
        builder.circle(parent_cx, parent_cy, node_radius + 4, "blue", "blue")?;
        builder.circle(current_cx, current_cy, node_radius + 2, "orange", "orange")?;
        builder.line(
            parent_cx,
            parent_cy,
            current_cx,
            current_cy,
            10,
            "rgba(0, 0, 5, 0.4)",
        )?;
    }
    for edge in edges.iter() {
        let (mut from_x, mut from_y) = from_flat_index(edge.head, width);
        from_x += offset;
        from_y += offset;
        let (mut to_x, mut to_y) = from_flat_index(edge.tail, width);
        to_x += offset;
        to_y += offset;
        builder.line(
            from_x * scale,
            from_y * scale,
            to_x * scale,
            to_y * scale,
            1,
            "blue",
        )?;
    }
    for node in nodes.iter() {
        let (mut cx, mut cy) = from_flat_index(node.id, width);
        cx += offset;
        cy += offset;
        let value = pixels[node.id];
        let color = if value > 0 { "lightgreen" } else { "yellow" };
        builder.circle(cx * scale, cy * scale, node_radius, "black", color)?;
    }
    if let Some(shortest_path) = shortest_path {
        for step in shortest_path.into_iter() {
            let (mut cx, mut cy) = from_flat_index(step, width);
            cx += offset;
            cy += offset;
            builder.circle(
                cx * scale,
                cy * scale,
                node_radius / 2,
                "black", 
                "red")?;
        }
    }
    let (mut search_cx, mut search_cy) = from_flat_index(search_index, width);
    search_cx += offset;
    search_cy += offset;
    builder.circle(
        search_cx * scale,
        search_cy * scale,
        node_radius / 2,
        "red",
        "red"
    )?;
    builder.build()?.flush()
}

fn to_flat_index(row: usize, column: usize, width: usize) -> usize {
    row * width + column
}

fn from_flat_index(index: usize, width: usize) -> (usize, usize) {
    (index % width, index / width)
}

enum Pixel {
    LeftTop,
    Top,
    RightTop,
    Left,
    Right,
    LeftBottom,
    Bottom,
    RightBottom,
    Middle,
}

fn identify_pixel(row_index: usize, column_index: usize, width: usize, height: usize) -> Pixel {
    match (row_index, column_index) {
        (0, 0) => Pixel::LeftTop,
        (row, 0) if row == height - 1 => Pixel::LeftBottom,
        (_, 0) => Pixel::Left,
        (0, column) if column == width - 1 => Pixel::RightTop,
        (row, column) if row == height - 1 && column == width - 1 => Pixel::RightBottom,
        (_, column) if column == width - 1 => Pixel::Right,
        (0, _) => Pixel::Top,
        (row, _) if row == height - 1 => Pixel::Bottom,
        _other => Pixel::Middle,
    }
}

fn build_graph_from_image<E, C: Fn(Node<u8>, Node<u8>) -> Edge<E>>(
    image: Image,
    converter: C,
) -> Option<Graph<u8, E>> {
    fn to_current_node(index: usize, data: &[u8]) -> Node<u8> {
        Node::<u8> {
            id: index,
            data: data[index],
        }
    }
    fn to_right_node(index: usize, data: &[u8]) -> Node<u8> {
        to_current_node(index + 1, data)
    }
    fn to_bottom_node(index: usize, width: usize, data: &[u8]) -> Node<u8> {
        to_current_node(index + width, data)
    }
    fn to_left_node(index: usize, data: &[u8]) -> Node<u8> {
        to_current_node(index - 1, data)
    }
    let edge = |head: Node<u8>, tail: Node<u8>| -> Edge<E> { converter(head, tail) };
    let build_edges = |width: usize,
                       height: usize,
                       row_index: usize,
                       column_index: usize,
                       data: &[u8]|
     -> Vec<Edge<E>> {
        let flat = to_flat_index(row_index, column_index, width);
        match identify_pixel(row_index, column_index, width, height) {
            Pixel::LeftTop => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_right = edge(
                    to_current_node(flat, data),
                    to_right_node(bottom.tail, data),
                );
                vec![right, bottom, bottom_right]
            }
            Pixel::LeftBottom => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                vec![right]
            }
            Pixel::Left => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_right = edge(
                    to_current_node(flat, data),
                    to_right_node(bottom.tail, data),
                );
                vec![right, bottom, bottom_right]
            }
            Pixel::RightTop => {
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_left =
                    edge(to_current_node(flat, data), to_left_node(bottom.tail, data));
                vec![bottom, bottom_left]
            }
            Pixel::RightBottom => {
                vec![]
            }
            Pixel::Right => {
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_left =
                    edge(to_current_node(flat, data), to_left_node(bottom.tail, data));
                vec![bottom, bottom_left]
            }
            Pixel::Top => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_left =
                    edge(to_current_node(flat, data), to_left_node(bottom.tail, data));
                let bottom_right = edge(
                    to_current_node(flat, data),
                    to_right_node(bottom.tail, data),
                );
                vec![right, bottom, bottom_left, bottom_right]
            }
            Pixel::Bottom => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                vec![right]
            }
            Pixel::Middle => {
                let right = edge(to_current_node(flat, data), to_right_node(flat, data));
                let bottom = edge(
                    to_current_node(flat, data),
                    to_bottom_node(flat, width, data),
                );
                let bottom_left =
                    edge(to_current_node(flat, data), to_left_node(bottom.tail, data));
                let bottom_right = edge(
                    to_current_node(flat, data),
                    to_right_node(bottom.tail, data),
                );
                vec![right, bottom, bottom_left, bottom_right]
            }
        }
    };

    let Image {
        data,
        width,
        height,
    } = image;
    let mut nodes = Vec::with_capacity(width * height);
    let mut edges = Vec::with_capacity(width * height);
    for row_index in 0..height {
        for column_index in 0..width {
            let flat_index = to_flat_index(row_index, column_index, width);
            let new_edges = build_edges(width, height, row_index, column_index, &data);
            let data = data[flat_index];
            edges.extend(new_edges);
            nodes.push(Node::<u8> {
                id: flat_index,
                data,
            });
        }
    }
    Some(Graph::<u8, E> { nodes, edges })
}

fn create_pixel_data(head: Node<u8>, tail: Node<u8>, width: usize) -> Edge<PixelData> {
    let height = head.data as i16 - tail.data as i16;
    let (head_row, head_column) = (head.id / width, head.id % width);
    let (tail_row, tail_column) = (tail.id / width, tail.id % width);
    let distance =
        (head_row.abs_diff(tail_row).pow(2) + head_column.abs_diff(tail_column).pow(2)) as f32;
    let distance = distance.sqrt();
    Edge {
        head: head.id,
        tail: tail.id,
        data: PixelData {
            height: height.into(),
            distance,
        },
    }
}

fn find_neighbours(
    source: Id,
    edges: &[Edge<PixelData>],
) -> impl Iterator<Item = (Id, &Edge<PixelData>)> {
    find_bidirectional_neighbours(source, edges).filter(|(_node, edge)| edge.data.height == 0)
}

fn main() {
    let image = read_image(Path::new("path.png")).expect("Failed to read image.");
    let image_width = image.width;
    let image_data = image.data.clone();
    let image_graph = build_graph_from_image::<PixelData, _>(image, |head, tail| {
        create_pixel_data(head, tail, image_width)
    })
    .expect("Failed to build graph from image.");
    let bidiractional_branching_fractor =
        graph::branching_factor(&image_graph, find_bidirectional_neighbours);
    let directional_branching_fractor =
        graph::branching_factor(&image_graph, find_directional_neighbours);
    println!("{image_graph}");
    println!("Bidiractional branching factor: {bidiractional_branching_fractor}");
    println!("Diractional branching factor: {bidiractional_branching_fractor}");
    let flat_start_index = to_flat_index(56, 54, image_width);
    let now = time::Instant::now();
    let image_bfs = breadth_first_search(flat_start_index, &image_graph.edges, find_neighbours);
    let search_index = to_flat_index(81, 72, image_width);
    let shortest_path =
        graph::build_shortest_path(flat_start_index, search_index, image_bfs.clone());
    let elapsed = now.elapsed();
    println!("Execution time: {} ms", elapsed.as_millis());
    write_image(
        Path::new("result.svg"),
        image_width,
        search_index,
        &image_graph.nodes,
        &image_graph.edges,
        image_data,
        image_bfs,
        shortest_path,
    )
    .expect("Failed to save result");
}
