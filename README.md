# svgraph

Program that reads `.png` and produces `.svg` file with the shortest path from __(x, y)__ to _(0, 0)_.

```
Graph(N: 10000, E: 39402)
Shortest path length: 60
Execution time: 17 ms

________________________________________________________
Executed in  599,27 millis    fish           external 
   usr time   76,55 millis    0,00 micros   76,55 millis 
   sys time  520,79 millis  1065,00 micros  519,73 millis
```

TODO:

* Introduce neighbour searcher.
* Consider distance between the nodes.
* Interactive UI.
* SVG: consider `def` element.
